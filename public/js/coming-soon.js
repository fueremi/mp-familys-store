(function($) {
  "use strict"; // Start of use strict

  // No JS

})(jQuery); // End of use strict

if(screen.width < 768){
  $('#whatsapp-hyperlink').attr('href', 'https://wa.me/6285156452146');
}else{
  $('#whatsapp-hyperlink').attr('href', 'https://web.whatsapp.com/send?phone=6285156452146');
}